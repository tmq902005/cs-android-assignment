package com.backbase.assignment.ui.custom

import android.annotation.TargetApi
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.graphics.RectF
import android.os.Build
import android.util.AttributeSet
import android.view.View
import androidx.core.content.ContextCompat
import com.backbase.assignment.R

class MyProgressBar : View {

    private var mArcPaintBackground: Paint? = null
    private var mArcPaintPrimary: Paint? = null
    private val mTextBounds = Rect()
    private var mProgress = 0
    private var mTextPaint: Paint? = null
    private var centerX = 0
    private var centerY = 0
    private var mWidthArcBG = 0
    private var mWidthAcrPrimary = 0
    private var mTextSizeProgress = 0

    constructor(context: Context) : super(context) {
        init(context)
    }

    constructor(
        context: Context,
        attrs: AttributeSet?
    ) : super(context, attrs) {
        init(context)
    }

    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int
    ) : super(context, attrs, defStyleAttr) {
        init(context)
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes) {
        init(context)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val viewWidthHeight =
            MeasureSpec.getSize(resources.getDimensionPixelSize(R.dimen.width_height_progress))
        centerX = viewWidthHeight / 2
        centerY = viewWidthHeight / 2
        setMeasuredDimension(viewWidthHeight, viewWidthHeight)
    }

    private fun initPixelSize() {
        mWidthArcBG = resources.getDimensionPixelSize(R.dimen.width_arc_background)
        mWidthAcrPrimary = resources.getDimensionPixelSize(R.dimen.width_arc_primary)
        mTextSizeProgress = resources.getDimensionPixelSize(R.dimen.text_size_progress)
    }

    private fun init(ctx: Context) {
        initPixelSize()
        mArcPaintBackground = Paint()
        mArcPaintBackground?.isDither = true
        mArcPaintBackground?.style = Paint.Style.STROKE
        mArcPaintBackground?.color = ContextCompat.getColor(context, R.color.greenOpacity)
        mArcPaintBackground?.strokeWidth = mWidthArcBG.toFloat()
        mArcPaintBackground?.isAntiAlias = true
        mArcPaintPrimary = Paint()
        mArcPaintPrimary?.isDither = true
        mArcPaintPrimary?.style = Paint.Style.STROKE
        mArcPaintPrimary?.strokeCap = Paint.Cap.ROUND
        mArcPaintPrimary?.color = ContextCompat.getColor(context, R.color.green)
        mArcPaintPrimary?.strokeWidth = mWidthAcrPrimary.toFloat()
        mArcPaintPrimary?.isAntiAlias = true
        mTextPaint = Paint()
        mTextPaint?.isAntiAlias = true
        mTextPaint?.textSize = mTextSizeProgress.toFloat()
        mTextPaint?.style = Paint.Style.FILL
        mTextPaint?.color = ContextCompat.getColor(context, android.R.color.black)
        mTextPaint?.strokeWidth = 2f
    }

    fun setProgress(progress: Int) {
        mProgress = progress
        invalidate()
        requestLayout()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        val rect = RectF(
            (0 + 10).toFloat(),
            (0 + 10).toFloat(),
            (canvas.width - 10).toFloat(),
            (canvas.height - 10).toFloat()
        )
        if(mProgress < 50){
            mArcPaintBackground?.color = ContextCompat.getColor(context, R.color.yellowOpacity)
            mArcPaintPrimary?.color = ContextCompat.getColor(context, R.color.yellow)
        }else{
            mArcPaintBackground?.color = ContextCompat.getColor(context, R.color.greenOpacity)
            mArcPaintPrimary?.color = ContextCompat.getColor(context, R.color.green)
        }
        canvas.drawArc(rect, 270f, 360f, false, mArcPaintBackground!!)
        canvas.drawArc(rect, 270f, (360 * (mProgress / 100f)), false, mArcPaintPrimary!!)
    }

}