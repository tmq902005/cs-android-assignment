package com.backbase.assignment.ui.custom

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Point
import android.os.Handler
import android.os.Looper
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.backbase.assignment.R
import com.backbase.assignment.ui.MainActivity
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import kotlinx.android.synthetic.main.detail_view.view.*
import java.lang.reflect.InvocationTargetException
import java.net.URL
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.Executors
import kotlin.math.roundToInt

class DetailView : ConstraintLayout {

    private var mView: View? = null

    private var curId = ""

    private var posterImg: Bitmap? = null

    private val executors = Executors.newSingleThreadExecutor()

    private var handlerMain = Handler(Looper.getMainLooper())

    private val defaultDateFMT = SimpleDateFormat("MMMM dd,yyyy",Locale.US)


    constructor(context: Context) : super(context) {
        initView(context, null)
    }

    constructor(context: Context, attributeSet: AttributeSet?) : super(context, attributeSet) {
        initView(context, attributeSet)
    }

    private fun initView(context: Context, attributeSet: AttributeSet?) {
        LayoutInflater.from(context).inflate(R.layout.detail_view, this)
        mView = rootView
        rootView.setOnClickListener { }
        detailView_actionBack.setOnClickListener {
            mView?.gone()
        }
    }

    private fun setData(
        id: String,
        nameFilm: String,
        dateFilm: String,
        contentFilm: String,
        posterPath: String
    ) {
        mView?.show()
        val animShow = AnimationUtils.loadAnimation(context, R.anim.fade_in)
        mView?.startAnimation(animShow)
        if (id != curId || posterImg == null) {
            executors.submit {
                val url =
                    URL("https://image.tmdb.org/t/p/original/$posterPath")
                val bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream())
                val point = Point()
                detailView_img.display.getSize(point)
                val w = point.x
                val h = point.x * bitmap.height / bitmap.width
                posterImg = Bitmap.createScaledBitmap(bitmap, w, h, false)
                handlerMain.post {
                    detailView_img.setImageBitmap(posterImg)
                }
            }
        } else {
            detailView_img.setImageBitmap(posterImg)
        }
        curId = id
        detailView_labelName.text = nameFilm
        detailView_labelDate.text = dateFilm
        detailView_labelContent.text = contentFilm
    }

    fun setData(data: JsonObject) {
        executors.submit {
            val jsonString = URL("https://api.themoviedb.org/3/movie/${data["id"]}?api_key=${MainActivity.YOUR_KEY}").readText()
            val detailData = JsonParser.parseString(jsonString).asJsonObject
            val nameFilm = detailData["title"].asString
            val dateFilm = try {
                val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)
                val date : Date? = dateFormat.parse(detailData["release_date"].asString)
                defaultDateFMT.format(date!!)
            } catch (e: ParseException) {
                e.printStackTrace()
                ""
            } catch (e: InvocationTargetException) {
                e.printStackTrace()
                ""
            }
            val contentFilm = detailData["overview"].asString
            val id = detailData["id"].asString
            val posterPath = detailData["poster_path"].asString
            val genres = detailData["genres"].asJsonArray
            val listGenres = arrayListOf<TextView>()
            detailView_listGen.removeAllViewsInLayout()
            genres.forEach {
                it as JsonObject
                val textView = TextView(context).apply {
                    layoutParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    ).apply {
                        setMargins(0, 10, 10, 0)
                        setPadding(10, 10, 10, 10)
                    }
                    text = it["name"].asString
                    textSize = resources.getDimension(R.dimen._6ssp)
                    this.setTextColor(resources.getColor(R.color.black))
                    background = ContextCompat.getDrawable(context,R.drawable.bg_border)
                }


                listGenres.add(textView)
            }

            handlerMain.post {
                listGenres.forEach {
                    detailView_listGen.addView(it)
                }
                detailView_img.setImageBitmap(null)
                setData(id,nameFilm, dateFilm, contentFilm,posterPath)
            }

        }

    }

    fun View.show() {
        this.visibility = View.VISIBLE
    }

    fun View.hide() {
        this.visibility = View.INVISIBLE
    }

    fun View.gone() {
        this.visibility = View.GONE
    }

}