package com.backbase.assignment.ui.movie

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Point
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.backbase.assignment.R
import com.backbase.assignment.ui.custom.RatingView
import com.backbase.assignment.ui.interfaces.ItemClickListener
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.google.gson.JsonObject
import java.lang.reflect.InvocationTargetException
import java.net.URL
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import kotlin.math.roundToInt


class MoviesAdapter(
    var items: ArrayList<JsonObject> = arrayListOf(),
    val listener: ItemClickListener
) :
    RecyclerView.Adapter<MoviesAdapter.ViewHolder>() {

    private val executors = Executors.newSingleThreadExecutor()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.movie_item,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(items[position], executors, listener)

    override fun getItemCount() = items.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val defaultDateFMT = SimpleDateFormat("MMMM dd,yyyy", Locale.US)
        lateinit var poster: ImageView
        lateinit var title: TextView
        lateinit var releaseDate: TextView
        lateinit var duration: TextView
        lateinit var rating: RatingView
        private var posterImg: Bitmap? = null
        private var curID = ""

        private var handlerMain = Handler(Looper.getMainLooper())
        lateinit var itemData: JsonObject

        fun bind(item: JsonObject, executors: ExecutorService, listener: ItemClickListener) =
            with(itemView) {
                itemData = item
                poster = itemView.findViewById(R.id.poster)
                poster.setImageDrawable(null)
                itemView.setOnClickListener {
                    listener.onClickListener(itemData)
                }
                //val id = item["id"].asString
                Glide.with(itemView)
                    .load("https://image.tmdb.org/t/p/w500/${item["poster_path"].asString}")
                    .centerCrop()
                    .into(poster)
                /*if (posterImg == null || curID != id) {
                    executors.submit {
                        val url =
                            URL("https://image.tmdb.org/t/p/original/${item["poster_path"].asString}")
                        val bitmap =
                            BitmapFactory.decodeStream(url.openConnection().getInputStream())
                        val point = Point()
                        poster.display.getSize(point)
                        val w = point.x
                        val h = point.x * bitmap.height / bitmap.width
                        posterImg = Bitmap.createScaledBitmap(bitmap, w, h, false)
                        handlerMain.post {
                            poster.setImageBitmap(posterImg)
                        }
                    }
                    curID = id
                } else {
                    poster.setImageBitmap(posterImg)
                }*/
                val rate = item["vote_average"].asFloat * 10
                rating = itemView.findViewById(R.id.rating)
                rating.setProgressBar(rate.roundToInt())

                title = itemView.findViewById(R.id.title)
                title.text = item["title"].asString
                releaseDate = itemView.findViewById(R.id.releaseDate)
                val date = try {
                    val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)
                    val date: Date? = dateFormat.parse(item["release_date"].asString)
                    defaultDateFMT.format(date!!)
                } catch (e: ParseException) {
                    e.printStackTrace()
                    ""
                } catch (e: InvocationTargetException) {
                    e.printStackTrace()
                    ""
                }
                releaseDate.text = date

                duration = itemView.findViewById(R.id.time)
                duration.text = item["popularity"].asString

            }
    }
}