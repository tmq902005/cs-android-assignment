package com.backbase.assignment.ui.interfaces

import com.google.gson.JsonObject

interface ItemClickListener{
    fun onClickListener(item: JsonObject)
}