package com.backbase.assignment.ui

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.backbase.assignment.R
import com.backbase.assignment.ui.interfaces.ItemClickListener
import com.backbase.assignment.ui.movie.MoviesAdapter
import com.backbase.assignment.ui.movie.MoviesHAdapter
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import kotlinx.android.synthetic.main.activity_main.*
import java.net.URL
import java.util.*
import java.util.Collections.sort
import java.util.concurrent.Executors


class MainActivity : AppCompatActivity() {
    companion object {
        const val TAG = "MainActivity"
        const val BASE_URL = "https://api.themoviedb.org/3"
        const val YOUR_KEY = "55957fcf3ba81b137f8fc01ac5a31fb5"
    }

    private lateinit var moviesAdapter: MoviesAdapter
    private lateinit var moviesAdapterH: MoviesHAdapter
    private lateinit var rvV: RecyclerView
    private lateinit var rvH: RecyclerView
    private val executors = Executors.newSingleThreadExecutor()
    private var curPageV = 0
    private var isLoadingMore = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rvV = findViewById(R.id.actMainV)
        rvH = findViewById(R.id.actMainH)
        rvV.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rvV.layoutManager!!.isMeasurementCacheEnabled = true
        rvH.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        val itemClickListener = object : ItemClickListener{
            override fun onClickListener(item: JsonObject) {
                actMainDetail.setData(item)
            }

        }

        moviesAdapter = MoviesAdapter(listener = itemClickListener)
        moviesAdapterH = MoviesHAdapter(listener = itemClickListener)
        rvV.adapter = moviesAdapter
        rvH.adapter = moviesAdapterH
        executors.submit {
            fetchMovies()
            updatePage()
        }

        rvV.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                Log.e(TAG, "New State: $newState")
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                Log.e(TAG, "dx: $dx dy: $dy")

                val layoutManager =
                    LinearLayoutManager::class.java.cast(recyclerView.layoutManager) ?: return
                val totalItemCount = layoutManager.itemCount
                val lastVisible = layoutManager.findLastVisibleItemPosition()

                val endHasBeenReached = lastVisible + 5 >= totalItemCount
                if (totalItemCount > 0 && endHasBeenReached && !isLoadingMore) {
                    isLoadingMore = true
                    executors.submit {
                        updatePage()
                    }

                }
            }
        })

    }

    private fun fetchMovies() {
        val jsonString =
            URL("$BASE_URL/movie/now_playing?language=en-US&page=undefined&api_key=$YOUR_KEY").readText()
        val jsonObject = JsonParser.parseString(jsonString).asJsonObject

        moviesAdapterH.items = jsonObject["results"] as JsonArray
        runOnUiThread {
            moviesAdapterH.notifyDataSetChanged()
        }

    }

    private fun updatePage(){
        curPageV++
        runOnUiThread {
            Toast.makeText(this@MainActivity, "Page: $curPageV - Loading... ", Toast.LENGTH_SHORT).show()
        }
        val jsonStringV = URL("$BASE_URL/movie/popular?api_key=$YOUR_KEY&language=en-US&page=$curPageV").readText()
        val jsonObjectV = JsonParser.parseString(jsonStringV).asJsonObject
        val arrResult = jsonObjectV["results"] as JsonArray
        val listJsonObject = ArrayList<JsonObject>()
        arrResult.forEach {
            val data = it.asJsonObject
            listJsonObject.add(data)
        }
        sort(listJsonObject,object :Comparator<JsonObject> {
            override fun compare(a: JsonObject, b: JsonObject): Int {
                val valA: Float = a["popularity"].asFloat
                val valB: Float = b["popularity"].asFloat
                if(valA > valB) return -1
                if(valA < valB) return 1
                return 0
            }
        })
        moviesAdapter.items.addAll(listJsonObject)
        runOnUiThread {
            moviesAdapter.notifyDataSetChanged()
            isLoadingMore = false
        }
    }

    override fun onDestroy() {
        executors.shutdown()
        super.onDestroy()
    }
}
