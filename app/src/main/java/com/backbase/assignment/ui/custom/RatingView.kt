package com.backbase.assignment.ui.custom

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.backbase.assignment.R
import kotlinx.android.synthetic.main.rating_view.view.*

class RatingView : ConstraintLayout{

    constructor(context: Context): super(context){
        initView(context, null)
    }

    constructor(context: Context, attributeSet: AttributeSet?): super(context, attributeSet){
        initView(context, attributeSet)
    }

    private fun initView(context: Context, attributeSet: AttributeSet?){
        LayoutInflater.from(context).inflate(R.layout.rating_view, this)
    }

    fun setProgressBar(number :Int){
        ratingView_progressBar.setProgress(number)
        ratingView_labelNumber.text = "$number"

    }

}