package com.backbase.assignment.ui.movie

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Point
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.backbase.assignment.R
import com.backbase.assignment.ui.interfaces.ItemClickListener
import com.bumptech.glide.Glide
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import java.net.URL
import java.util.concurrent.Executors

class MoviesHAdapter(var items: JsonArray = JsonArray(), private val listener: ItemClickListener) :
    RecyclerView.Adapter<MoviesHAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.movie_item_h,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(items[position] as JsonObject, listener)

    override fun getItemCount() = items.size()

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        lateinit var poster: ImageView

        fun bind(item: JsonObject, listener: ItemClickListener): Unit = with(itemView) {
            itemView.setOnClickListener {
                listener.onClickListener(item)
            }
            poster = itemView.findViewById(R.id.poster)
            Glide.with(itemView)
                .load("https://image.tmdb.org/t/p/w500/${item["poster_path"].asString}")
                .centerCrop()
                .into(poster)
        }
    }
}